<?php get_header(); ?>



            <div class="nrg-header">
                <header><h2>NRG, for an adventurous world</h2></header>
                <div class="nrg-button">
                    <button>Learn More</button>
                </div>
            </div>

            <div class="nrg-hero">
                <div class="nrg-hero-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/drone.jpg">
                </div>
                <div class="nrg-hero-text">
                    <h3>Drone Battery Life <br />Unleashed</h3>
                    <button>Shop Now</button>
                </div>
            </div>

            <div class="nrg-package">
                <div class="nrg-package-wrapper">
                    <h3>Lithium Polymer Batteries</h3>
                    <div class="all-nrg-packages">
                        <div class="product-package">
                        <h4>Product Name 1</h4>
                        <h6>$25.95 each</h6>
                        <button>View Now</button>
                    </div>
                    <div class="product-package">
                        <h4>Product Name 2</h4>
                        <h6>$25.95 each</h6>
                        <button>View Now</button>
                    </div>
                    <div class="product-package">
                        <h4>Product Name 1</h4>
                        <h6>$25.95 each</h6>
                        <button>View Now</button>
                    </div>
                    <div class="product-package">
                        <h4>Product Name 1</h4>
                        <h6>$25.95 each</h6>
                        <button>View Now</button>
                    </div>
                    </div>
                     <p class="see-all">see all NRG products (10 more) ></p>
                </div>

                <img src="<?php echo get_template_directory_uri(); ?>/img/nrg-package.png">
            </div>

            <div class="nrg-specs">
                <div class ="nrg-specs-wrapper">
                    <h3>Specs</h3>
                    <div class="spec-block">
                        <h6>Lorem Ipsum</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur <br> adipisicing elit, sed do eiusmod.</p>
                    </div>
                    <div class="spec-block">
                        <h6>Lorem Ipsum</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur <br> adipisicing elit, sed do eiusmod.</p>
                    </div>
                    <div class="spec-block">
                        <h6>Lorem Ipsum</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur <br> adipisicing elit, sed do eiusmod.</p>
                    </div>
                    <div class="spec-block">
                        <h6>Lorem Ipsum</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur <br> adipisicing elit, sed do eiusmod.</p>
                    </div>
                    <div class="spec-block">
                        <h6>Lorem Ipsum</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur <br> adipisicing elit, sed do eiusmod.</p>
                    </div>
                    <div class="spec-block">
                        <h6>Lorem Ipsum</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur <br> adipisicing elit, sed do eiusmod.</p>
                    </div>
                </div>      
            </div>

            <div class="nrg-contact">
                <div class="nrg-contact-wrapper">
                    <h3>Get in Touch to Learn More</h3>

                    <div class="contact-form-block">
                        <form id="contact_form" action="#" method="POST" enctype="multipart/form-data">

                            <div class="row">
                                <label for="message">Your Message</label><br />
                                <textarea id="message" class="input" name="message"  placeholder="I'm wondering about..."></textarea><br />
                            </div>

                            <div>
                               <div class="row row-input">
                                <label for="name">Name & Surname</label><br />
                                <input id="name" class="input" name="name" type="text" value="" placeholder="Your name"/><br />
                            </div>
                            <div class="row row-input">
                                <label for="email">Email Address</label><br />
                                <input id="email" class="input" name="email" type="text" value="" placeholder="name@domain.com" /><br />
                            </div>
                            </div>

                            <div class="row row-submit">
                                <input id="submit-button" type="submit" value="Send That Message" />
                            </div>
                            
                        </form>                     
                    </div>
                </div>
            </div>
              
        </div>

<?php get_footer(); ?>
