<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<link href='https://fonts.googleapis.com/css?family=Work+Sans:400,200,100' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Ruda:400,700|Rubik:400,500,700,300' rel='stylesheet' type='text/css'>

		<?php wp_head(); ?>
		<script src="js/vendor/modernizr-2.8.3.min.js"></script>

	</head>
	<body <?php body_class(); ?>>

   <div class="page-wrap">

            <div class="top-bar">
                <div class="top-bar-wrapper">

                    <div class="nrg-logo">
                        <img class="top-logo" src="<?php echo get_template_directory_uri(); ?>/img/nrg-logo.png">
                    </div>
                    <nav class="nrg-nav">
                        <ul>
                            <li><a href="/">Discover</a></li>
                            <li><a href="/">Products</a></li>
                            <li><a href="/">Specs</a></li>
                            <li><a href="/">Blog</a></li>
                            <li><a href="/">Contact</a></li>
                        </ul>
                    </nav>

                </div>
            </div> <!-- .top-bar -->
