<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'nrg_local');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'u >s+>3Aro;|LKxA>9HUy+>Twmt]pf,&7Ib<8@#v&x@T<f7D~_Fj`s<^!q,aR-ve');
define('SECURE_AUTH_KEY',  '.4koZp,c?h.92|epnzugV/6%7o=bf7J=l`:Z>G.a0paRCid6h8FNjUg`UsU_}nR/');
define('LOGGED_IN_KEY',    '@ts]sp`33T}-%(^rk=8AYhh9<kl[5I-Qt(1U_5dEwFf4fmlF+ix1DOys+ C$P`&H');
define('NONCE_KEY',        'P;c{pKLZL+>+4xQrjkeQ3I!cnxRS*|c>m-+`r=NcCl5kMikZqC~<h+bD<x@q8<ud');
define('AUTH_SALT',        'V`mR@YPnAR>Y%v`-%=Qf5(eyOER-MeuCwr&VNkFj|sc/C,dT_W3a3USMzQl 8/De');
define('SECURE_AUTH_SALT', 'm>F5l||T.f;pxoPm|uFzbnDfyoRy}=dtNaV<v|@})ta$k5?f+.sOtP.^U12gNpoi');
define('LOGGED_IN_SALT',   '#oLAXJ1;;ZBfdav%^-+hJ>_DWo>Y39J$c}VRHku@tQtJq,bX;@,[SgRrm$<1XiCN');
define('NONCE_SALT',       '~)<{D-@#uA!@%>,_c$J*De:+W+rX2ME3OO&#[16X?*S@uKf$Mh~MK I0n^Sp q0H');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
